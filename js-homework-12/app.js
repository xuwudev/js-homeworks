document.addEventListener("DOMContentLoaded", function () {
  const buttons = document.querySelectorAll(".btn")
  let lastClickedBtn = null

  buttons.forEach((button) => {
    button.addEventListener("click", function () {
      if (lastClickedBtn) {
        lastClickedBtn.style.backgroundColor = "#000000"
      }
      this.style.backgroundColor = "blue"
      lastClickedBtn = this
    })
  })

  document.addEventListener("keypress", function (event) {
    const key = event.key.toUpperCase()
    if (key === "ENTER") {
      event.preventDefault()
    }
    const targetBtn = Array.from(buttons).find((btn) => btn.innerText === key)
    if (targetBtn) {
      if (key === "ENTER") {
        if (lastClickedBtn) {
          lastClickedBtn.style.backgroundColor = "#000000"
        }
      } else {
        if (lastClickedBtn) {
          lastClickedBtn.style.backgroundColor = "#000000"
        }
        targetBtn.style.backgroundColor = "blue"
        lastClickedBtn = targetBtn
      }
    }
  })
})
