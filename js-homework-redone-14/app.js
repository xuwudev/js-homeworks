const toggleBtn = document.getElementById("toggle-btn")
const theme = document.getElementById("theme")
let darkMode = localStorage.getItem("dark-mode")
const tabs = document.querySelectorAll(".tabs-title")

const enableDarkMode = () => {
  theme.classList.add("dark-mode-theme")
  toggleBtn.classList.remove("dark-mode-toggle")
  localStorage.setItem("dark-mode", "enabled")
  tabs.forEach((tab) => {
    tab.classList.add("dark-mode-theme")
  })
}

const disableDarkMode = () => {
  theme.classList.remove("dark-mode-theme")
  toggleBtn.classList.add("dark-mode-toggle")
  localStorage.setItem("dark-mode", "disabled")
  tabs.forEach((tab) => {
    tab.classList.remove("dark-mode-theme")
  })
}

if (darkMode === "enabled") {
  enableDarkMode() // set state of darkMode on page load
}

toggleBtn.addEventListener("click", (e) => {
  darkMode = localStorage.getItem("dark-mode") // update darkMode when clicked
  if (darkMode === "disabled") {
    enableDarkMode()
  } else {
    disableDarkMode()
  }
})

document.addEventListener("DOMContentLoaded", function () {
  const tabs = document.querySelectorAll(".tabs-title")
  const tabContents = document.querySelectorAll(".tabs-content li")

  tabs.forEach((tab, index) => {
    tab.addEventListener("click", () => {
      tabs.forEach((tab) => tab.classList.remove("active"))
      tab.classList.add("active")

      tabContents.forEach((content) => (content.style.display = "none"))
      tabContents[index].style.display = "block"
    })
  })
})
