document
  .querySelector(".password-form")
  .addEventListener("click", function (event) {
    if (event.target.classList.contains("icon-password")) {
      const inputField = event.target.previousElementSibling

      if (inputField.type === "password") {
        inputField.type = "text"
        event.target.classList.remove("fa-eye")
        event.target.classList.add("fa-eye-slash")
      } else {
        inputField.type = "password"
        event.target.classList.remove("fa-eye-slash")
        event.target.classList.add("fa-eye")
      }
    }
  })

document.querySelector(".btn").addEventListener("click", function () {
  const password1 = document.querySelector(".pass1")

  const password2 = document.querySelector(".pass2")

  if (password1.value === password2.value) {
    alert("You are welcome")
  } else {
    const errorText = document.createElement("p")
    errorText.textContent = "Потрібно ввести однакові значення"
    errorText.style.color = "red"
    document.querySelector(".password-form").appendChild(errorText)
  }
})
