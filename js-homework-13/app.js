document.addEventListener("DOMContentLoaded", function () {
  const images = document.querySelectorAll(".image-to-show")
  const pauseResumeButton = document.createElement("button")
  pauseResumeButton.textContent = "Припинити"
  document.body.appendChild(pauseResumeButton)

  let currentIndex = 0
  let intervalId
  let timer

  function showImage() {
    hideAllImages()
    images[currentIndex].style.display = "block"
    timer = 3
    updateTimer()

    intervalId = setInterval(function () {
      if (timer > 0) {
        timer--
        updateTimer()
      } else {
        clearInterval(intervalId)
        hideAllImages()
        currentIndex = (currentIndex + 1) % images.length
        showImage()
      }
    }, 1000)
  }

  function hideAllImages() {
    images.forEach((image) => {
      image.style.display = "none"
    })
  }

  function updateTimer() {
    pauseResumeButton.textContent = `Час до наступної картинки: ${timer} секунди`
  }

  function pauseSlideshow() {
    clearInterval(intervalId)
    pauseResumeButton.textContent = "Відновити показ"
    pauseResumeButton.removeEventListener("click", pauseSlideshow)
    pauseResumeButton.addEventListener("click", resumeSlideshow)
  }

  function resumeSlideshow() {
    showImage()
    pauseResumeButton.textContent = "Припинити"
    pauseResumeButton.removeEventListener("click", resumeSlideshow)
    pauseResumeButton.addEventListener("click", pauseSlideshow)
  }

  pauseResumeButton.addEventListener("click", pauseSlideshow)
  showImage()
})
