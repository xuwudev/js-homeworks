"use strict"

const pass = document.getElementById("pass")
const passConfirm = document.getElementById("pass-confirm")
const label = document.getElementsByClassName("input-wrapper")[1]
const span = document.createElement("span")
span.textContent = "You need to enter the same values"
span.style.cssText = "display: none; color: black"
span.style.padding = "20px"
label.append(span)

const confirm = document.getElementsByClassName("btn")[0]

confirm.addEventListener("click", () => {
  passConfirm.value !== pass.value
    ? (span.style.display = "flex")
    : (span.style.display = "none")
  pass.value === passConfirm.value &&
  (pass.value !== "" || passConfirm.value !== "")
    ? alert("You are welcome!")
    : false
})

const opened = document.getElementsByClassName("icon-password")[0]
const hidden = document.getElementsByClassName("icon-password")[1]
const opened2 = document.getElementsByClassName("icon-password")[2]
const hidden2 = document.getElementsByClassName("icon-password")[3]

opened.addEventListener("click", function hideFirst() {
  pass.type = "text"
  hidden.style.display = "inline"
  opened.style.display = "none"
  hidden.addEventListener("click", openFirst)
})

function openFirst() {
  pass.type = "password"
  opened.style.display = "inline"
  hidden.style.display = "none"
}

opened2.addEventListener("click", function hideSecond() {
  passConfirm.type = "text"
  hidden2.style.display = "inline"
  opened2.style.display = "none"
  hidden2.addEventListener("click", openSecond)
})

function openSecond() {
  passConfirm.type = "password"
  opened2.style.display = "inline"
  hidden2.style.display = "none"
}

pass.addEventListener("blur", () => {
  if (pass.value === "") {
    opened.style.display = "inline"
    hidden.style.display = "none"
    pass.type = "password"
  } else {
    passConfirm.disabled = false
    opened2.style.display = "inline"
    hidden2.style.display = "none"
  }
})
passConfirm.addEventListener("blur", () => {
  if (passConfirm.value === "") {
    opened2.style.display = "inline"
    hidden2.style.display = "none"
    passConfirm.type = "password"
  }
})

passConfirm.addEventListener("click", () => {
  if (pass.value === "") {
    passConfirm.disabled = true
    passConfirm.value = ""
    opened2.style.display = "none"
    hidden2.style.display = "none"
  }
})

let darkIcon = document.getElementById("darkicon")
let lightIcon = document.getElementById("lighticon")
let submit = document.getElementById("submit")
let input = document.querySelectorAll("input")
let inputArray = Array.from(input)
let eye = document.querySelectorAll("i")
let eyeArray = Array.from(eye)

lightIcon.classList.add("hide-icon")
submit.style.background = "white"
submit.style.color = "black"
inputArray.forEach((item) => {
  item.style.background = "white"
  item.style.color = "black"
})

function darkMode() {
  darkIcon.classList.add("hide-icon")
  lightIcon.classList.remove("hide-icon")
  document.body.style.background = "black"
  document.body.style.color = "white"
  submit.style.background = "black"
  submit.style.color = "white"
  eyeArray.forEach((item) => {
    item.style.color = "black"
    span.textContent = "You need to enter the same values"
    span.style.cssText = "display: none; color: white"
    span.style.padding = "20px"
    label.append(span)
  })
}

function lightMode() {
  lightIcon.classList.add("hide-icon")
  darkIcon.classList.remove("hide-icon")
  document.body.style.background = "white"
  document.body.style.color = "black"
  submit.style.background = "white"
  submit.style.color = "black"
  inputArray.forEach((item) => {
    item.style.background = "white"
    item.style.color = "black"
  })
  eyeArray.forEach((item) => {
    item.style.color = "pink"
  })
}
